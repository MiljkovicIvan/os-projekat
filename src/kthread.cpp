#include <kthread.h>
#include <pcb.h>
#include <dos.h>
#include <kernel.h>
#include <syscalls.h>
#include <lock.h>
#include <iostream.h>

KernelThread::KernelThread() {
    stackSize = 4096;
    pcb = new PCB(stackSize, 0);
    stackSize = defaultStackSize / sizeof(unsigned);
    pcb->stack = new unsigned[stackSize];

    pcb->ss = FP_SEG(pcb->stack + stackSize);
    pcb->so = FP_OFF(pcb->stack + stackSize);
}

void KernelThread::takeOver(unsigned callID, unsigned seg, unsigned off) {

    unsigned* sp = pcb->stack + stackSize;

    *(--sp) = seg;
    *(--sp) = off;
    *(--sp) = callID;

    *(--sp) = FP_SEG(Kernel::switchContext);
    *(--sp) = FP_OFF(Kernel::switchContext);

    *(--sp) = 0x200;
    *(--sp) = FP_SEG(Kernel::callRoutine);
    *(--sp) = FP_OFF(Kernel::callRoutine);


    _SP = FP_OFF(sp);
    _SS = FP_SEG(sp);

    asm iret;
}

KernelThread::~KernelThread() {
    delete pcb;
}

