#include <thread.h>
#include <DOS.H>
#include <types.h>
#include <kernel.h>

Thread::Thread(StackSize stackSize, Time timeSlice) {
    ThreadInfo ti;
    ThreadInfo * pti = &ti;

    pti->stackSize = stackSize;
    pti->timeSlice = timeSlice;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_newThread;
    _BX = FP_SEG(pti);
    _CX = FP_OFF(pti);

    asm int 60h;

    tid = pti->tid;

    asm {
        pop cx
        pop bx
        pop ax
    }
}

Thread::~Thread() {
    waitToComplete();
}

void Thread::start() {
    ThreadInfo ti;
    ThreadInfo* pti = &ti;

    pti->tid = tid;
    pti->myThread = this;
    pti->run = Thread::wrapper;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_startThread;
    _BX = FP_SEG(pti);
    _CX = FP_OFF(pti);

    asm int 60h

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void Thread::waitToComplete() {
    ThreadInfo ti;
    ThreadInfo* pti = &ti;

    pti->tid = tid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_waitToComplete;
    _BX = FP_SEG(pti);
    _CX = FP_OFF(pti);

    asm int 60h

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void Thread::sleep(Time timeToSleep) {

    if (timeToSleep <= 0)
        return;

    ThreadInfo ti;
    ThreadInfo* pti = &ti;

    pti->tid = Kernel::running->pid;
    pti->timeSlice = timeToSleep;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_sleepThread;
    _BX = FP_SEG(pti);
    _CX = FP_OFF(pti);

    asm int 60h

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void Thread::wrapper(Thread* running) {

    running->run();

    ThreadInfo ti;
    ThreadInfo* pti = &ti;

    pti->tid = running->tid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_endThread;
    _BX = FP_SEG(pti);
    _CX = FP_OFF(pti);

    asm int 60h

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void dispatch() {
    _AX = sysID_dispatch;

    asm int 60h
}

