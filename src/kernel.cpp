#include <kernel.h>
#include <iostream.h>
#include <vector.h>
#include <dos.h>
#include <types.h>
#include <kthread.h>
#include <syscalls.h>
#include <debug.h>
#include <idle.h>
#include <squeue.h>
#include <kersem.h>
#include <kernev.h>
#include <schedule.h>
#include <lock.h>


Vector<PCB*>* Kernel::pcbVect = 0;
Vector<KerSem*>* Kernel::semVect = 0;
Vector<KernelEvent*>* Kernel::eventVect = 0;


PCB* Kernel::running = 0;
PCB* Kernel::userMain = 0;


IdleThread* Kernel::idleThread = 0;
KernelThread* Kernel::kernelThread = 0;


KernelState Kernel::mode;


void* Kernel::oldTimer = 0;


SleepingQueue* Kernel::sleepingQueue = 0;


int Kernel::tick = 0;

extern void tick();

int Kernel::eventFlag = 0;


pFun* Kernel::funs = 0;


void interrupt syscall(unsigned p_bp, unsigned p_di, unsigned p_si, unsigned p_ds, unsigned p_es, unsigned p_dx, unsigned p_cx, unsigned p_bx, unsigned p_ax, unsigned p_ip, unsigned p_cs, unsigned flags) {

    Kernel::running->ss = _SS;
    Kernel::running->so = _SP;

    Kernel::mode = kernel; /* entering kernel mode */

    Kernel::kernelThread->takeOver(p_ax, p_bx, p_cx);

}

void interrupt timer() {

    if (!switch_context_request)
        asm int 61h;

    tick();

    if (switch_context_request)
        if (Kernel::running->timeSlice != 0)
            Kernel::tick--;

    Kernel::sleepingQueue->update();

#ifdef timer_debug
    lock_flag = 0;
    cout << "------------------------------------------------" << endl;
    cout << "kernel::mode " << Kernel::mode << endl;
    cout << "kernel::tick " << Kernel::tick << endl;
    cout << "kernel::running->timeSlice " << Kernel::running->timeSlice << endl;
    cout << "------------------------------------------------" << endl;
    asm cli;
    lock_flag = 1;
#endif

    if ((Kernel::mode == idle)
        || (Kernel::mode == notKernel && Kernel::tick <=0 && Kernel::running->timeSlice != 0)
        || (Kernel::mode == notKernel && Kernel::eventFlag == 1)
        || (Kernel::mode == notKernel && switch_context_request)) {

        /* no need for dispatch() from thread.cpp since we're already in
         * interrupt routine */
        if (lock_flag) {
            Kernel::eventFlag = 0;

#ifdef timer_debug
            lock_flag = 0;
            cout << "[TIMER] saving context ss=" << _SS << ", sp=" << _SP << endl;
            asm cli;
            lock_flag = 1;
#endif

            Kernel::running->ss = _SS;
            Kernel::running->so = _SP;

            Kernel::mode = kernel; /* entering kernel mode */

            Kernel::kernelThread->takeOver(sysID_dispatch,0,0);
        }
        else
            switch_context_request = 1;

    }

}

void Kernel::init() {

    lock

    pcbVect = new Vector<PCB*>;
    semVect = new Vector<KerSem*>;
    eventVect = new Vector<KernelEvent*>;

    setvect(0x60, (void interrupt(*)(...))syscall);

    userMain = new PCB(defaultStackSize, defaultTimeSlice);
    userMain->pid = pcbVect->put(userMain);
    userMain->state = PCB_running;

    running = userMain;

    kernelThread = new KernelThread();
    idleThread = new IdleThread();

    sleepingQueue = new SleepingQueue();

    oldTimer = getvect(0x8);
    setvect(0x61, (void interrupt(*)(...))oldTimer);
    setvect(0x8, (void interrupt(*)(...))timer);

    funs = new pFun[15];

    funs[sysID_newThread] = SYS_newThread;
    funs[sysID_startThread] = SYS_startThread;
    funs[sysID_waitToComplete] = SYS_waitToComplete;
    funs[sysID_sleepThread] = SYS_sleepThread;
    funs[sysID_endThread] = SYS_endThread;
    funs[sysID_dispatch] = SYS_dispatch;

    funs[sysID_newSem] = SYS_newSemaphore;
    funs[sysID_SemWait] = SYS_SemaphoreWait;
    funs[sysID_SemSig] = SYS_SemaphoreSignal;
    funs[sysID_SemVal] = SYS_SemaphoreVal;
    funs[sysID_delSem] = SYS_deleteSemaphore;

    funs[sysID_newEv] = SYS_newEvent;
    funs[sysID_waitEv] = SYS_waitEvent;
    funs[sysID_sigEv] = SYS_signalEvent;
    funs[sysID_delEv] = SYS_deleteEvent;

    unlock

#ifdef kernel_debug
    lock_flag = 0;
    cout << "Initialazing kernel finished" << endl;
    asm cli;
    lock_flag = 1;
#endif

}

void Kernel::clear() {


    delete kernelThread;
    delete idleThread;

    setvect(0x8, (void interrupt(*)(...))oldTimer);

    delete semVect;
    delete pcbVect;
    delete eventVect;

    delete funs;

#ifdef kernel_debug
    lock_flag = 0;
    cout << "Clearing kernel finished" << endl;
    asm cli;
    lock_flag = 1;
#endif

}

void Kernel::switchContext() {

#ifdef despatch_debug
        lock_flag = 0;
        cout << "State of the old running PCB: " << PCBNames[Kernel::running->state] << endl;

        cout << "[DISPATCH] switching from: " << Kernel::running->pid;

        asm cli;
        lock_flag = 1;
#endif

    if (Kernel::running->state == PCB_ready)
        Scheduler::put(Kernel::running);

    if (Kernel::running->state != PCB_running)
        if ((Kernel::running = Scheduler::get()) == 0) {
            Kernel::idleThread->takeOver();
        } else
            Kernel::tick = Kernel::running->timeSlice;

    Kernel::running->state = PCB_running;

#ifdef dispatch_debug
    lock_flag = 0;
    cout << " to: " << Kernel::running->pid << endl;
    asm cli;
    lock_flag = 1;
#endif

    // end of dispatch

#ifdef sysCall
    lock_flag = 0;
    cout << "[switchContext, threadNo=" << Kernel::running->pid << "] switching to ss=" << Kernel::running->ss << ", sp=" << Kernel::running->so << endl;
    asm cli;
    lock_flag = 1;
#endif

    _SP = Kernel::running->so;
    _SS = Kernel::running->ss;

    Kernel::mode = notKernel;

    asm {
        pop bp
        pop di
        pop si
        pop ds
        pop es
        pop dx
        pop cx
        pop bx
        pop ax

        iret
    }
}

void Kernel::callRoutine(unsigned callID, void * data) {

#ifdef sysCall
    lock_flag = 0;
    cout << "[callRoutine] op: " << callIDs[callID] << endl;
    asm cli;
    lock_flag = 1;
#endif

    funs[callID](data);

}

