#include <event.h>
#include <types.h>
#include <dos.h>

Event::Event(IVTNo ivtno) {
    EvInfo ei;
    EvInfo* pei = &ei;

    pei->ivtno = ivtno;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_newEv;
    _BX = FP_SEG(pei);
    _CX = FP_OFF(pei);

    asm int 60h;

    eid = pei->eid;

    asm {
        pop cx
        pop bx
        pop ax
    }

}

Event::~Event() {
    EvInfo ei;
    EvInfo* pei = &ei;

    pei->eid = eid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_delEv;
    _BX = FP_SEG(pei);
    _CX = FP_OFF(pei);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }
}

void Event::wait() {
    EvInfo ei;
    EvInfo* pei = &ei;

    pei->eid = eid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_waitEv;
    _BX = FP_SEG(pei);
    _CX = FP_OFF(pei);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void Event::signal() {
    EvInfo ei;
    EvInfo* pei = &ei;

    pei->eid = eid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_sigEv;
    _BX = FP_SEG(pei);
    _CX = FP_OFF(pei);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }

}


