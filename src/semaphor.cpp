#include <semaphor.h>
#include <types.h>
#include <dos.h>

Semaphore::Semaphore(int init) {
    SemInfo si;
    SemInfo* psi = &si;

    psi->value = init;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_newSem;
    _BX = FP_SEG(psi);
    _CX = FP_OFF(psi);

    asm int 60h;

    sid = psi->sid;

    asm {
        pop cx
        pop bx
        pop ax
    }

}

void Semaphore::wait() {
    SemInfo si;
    SemInfo* psi = &si;

    psi->sid = sid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_SemWait;
    _BX = FP_SEG(psi);
    _CX = FP_OFF(psi);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }
}

void Semaphore::signal() {
    SemInfo si;
    SemInfo* psi = &si;

    psi->sid = sid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_SemSig;
    _BX = FP_SEG(psi);
    _CX = FP_OFF(psi);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }
}

int Semaphore::val() const {
    SemInfo si;
    SemInfo* psi = &si;

    psi->sid = sid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_SemVal;
    _BX = FP_SEG(psi);
    _CX = FP_OFF(psi);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }

    return psi->value;
}

Semaphore::~Semaphore() {
    SemInfo si;
    SemInfo* psi = &si;

    psi->sid = sid;

    asm {
        push ax
        push bx
        push cx
    }

    _AX = sysID_delSem;
    _BX = FP_SEG(psi);
    _CX = FP_OFF(psi);

    asm int 60h;

    asm {
        pop cx
        pop bx
        pop ax
    }
}

