#include <syscalls.h>
#include <types.h>
#include <pcb.h>
#include <vector.h>
#include <kernel.h>
#include <schedule.h>
#include <iostream.h>
#include <debug.h>
#include <kersem.h>
#include <kernev.h>
#include <lock.h>


void SYS_newThread(void* ptr) {
    ThreadInfo* t = (ThreadInfo*) ptr;
    PCB * pcb = new PCB(t->stackSize, t->timeSlice);
    pcb->pid = Kernel::pcbVect->put(pcb);
    t->tid = pcb->pid;
    pcb->state = PCB_created;

#ifdef thread
    lock_flag = 0;
    cout << "[Thread] Thread " << Kernel::running->pid << " creating thread " << pcb->pid << endl;
    asm cli;
    lock_flag = 1;
#endif
}


void SYS_startThread(void* ptr) {
    ThreadInfo* t = (ThreadInfo*) ptr;
    PCB * pcb = (*Kernel::pcbVect)[t->tid];
    pcb->initStack(t->myThread, t->run);

    if (pcb->state == PCB_created) {
        pcb->state = PCB_ready;
        Scheduler::put(pcb);
    } else {
        cout << "starting already started pcb" << endl;;
    }

#ifdef thread
    lock_flag = 0;
    cout << "[Thread] Thread " << Kernel::running->pid << " starting thread " << pcb->pid << endl;
    asm cli;
    lock_flag = 1;
#endif


}


void SYS_waitToComplete(void* ptr) {
    ThreadInfo* ti = (ThreadInfo*) ptr;
    PCB * pcb = (*Kernel::pcbVect)[ti->tid];

    if (pcb->state == PCB_blocked || pcb->state == PCB_ready) {
        pcb->blocked.put(Kernel::running);
#ifdef thread
        lock_flag = 0;
        cout << "[Thread] ThraedNo: " << Kernel::running->pid << " blocked in " << pcb->pid << "'s blocking list" << endl;
        asm cli;
        lock_flag = 1;
#endif
        Kernel::running->state = PCB_blocked;

        //SYS_dispatch();
        //dispatching after every system call
    }
#ifdef thread
    else {
        lock_flag = 0;
        cout << "[Thread] ThreadNo: " << pcb->pid << "is finished. state: " << PCBNames[pcb->state] << ", Kernel::running is not blocked" << endl;
    }
    asm cli;
    lock_flag = 1;
#endif

}


void SYS_dispatch(void*) {

    if (Kernel::running->state == PCB_running)
        Kernel::running->state = PCB_ready;

}


void SYS_sleepThread(void* ptr) {
    ThreadInfo* ti = (ThreadInfo*) ptr;
    Kernel::sleepingQueue->put(Kernel::running, ti->timeSlice); /* timeSlice is actually timeToSleep */
    if (Kernel::running->state == PCB_running)
        Kernel::running->state = PCB_blocked;

    //SYS_dispatch();
}


void SYS_endThread(void* ptr) {
    ThreadInfo* ti = (ThreadInfo*) ptr;
    PCB * pcb = (*Kernel::pcbVect)[ti->tid];

    pcb->state = PCB_finished;

    while (! pcb->blocked.empty()) {
        PCB * unblocked = pcb->blocked.get();

        if (unblocked->state == PCB_blocked) // reda radi :)
            unblocked->state = PCB_ready;
        else
            cout << "unblocking nonblocked pcb" << endl;

#ifdef thread
        lock_flag = 0;
        cout << "[Thread] Unblcking Thread: " << unblocked->pid << endl;
        asm cli;
        lock_flag = 1;
#endif
        Scheduler::put(unblocked);
    }

#ifdef thread
    lock_flag = 0;
    cout << "[Thread] ThreadNo: " << pcb->pid << " finished" << endl;
    asm cli;
    lock_flag = 1;
#endif

    //SYS_dispatch();
}



void SYS_newSemaphore(void* ptr){
    SemInfo* si = (SemInfo*) ptr;
    KerSem * sem = new KerSem(si->value);
    sem->sid = Kernel::semVect->put(sem);
    si->sid = sem->sid ;

#ifdef sem_debug
    lock_flag = 0;
    cout << "Semaphore " << si->sid << " created" << endl;
    asm cli;
    lock_flag = 1;
#endif
}


void SYS_SemaphoreWait(void* ptr){
    SemInfo* si = (SemInfo*) ptr;
    (*Kernel::semVect)[si->sid]->wait();
}


void SYS_SemaphoreSignal(void* ptr){
    SemInfo* si = (SemInfo*) ptr;
    (*Kernel::semVect)[si->sid]->signal();
}


void SYS_SemaphoreVal(void* ptr){
    SemInfo* si = (SemInfo*) ptr;
    si->value = (*Kernel::semVect)[si->sid]->value;
}


void SYS_deleteSemaphore(void* ptr){
    SemInfo* si = (SemInfo*) ptr;
    KerSem * sem = Kernel::semVect->get(si->sid);

    delete sem;
}



void SYS_newEvent(void* ptr) {
    EvInfo* data = (EvInfo*) ptr;
    KernelEvent* event = new KernelEvent(data->ivtno);
    data->eid = Kernel::eventVect->put(event);

#ifdef event_debug
    lock_flag = 0;
    cout << "[EVENT] " << data->eid << " created" << endl;
    asm cli;
    lock_flag = 1;
#endif
}


void SYS_waitEvent(void* ptr) {
    EvInfo* data = (EvInfo*) ptr;
    KernelEvent* event = (*Kernel::eventVect)[data->eid];
    event->wait();
}


void SYS_signalEvent(void* ptr) {
    EvInfo* data = (EvInfo*) ptr;
    KernelEvent* event = (*Kernel::eventVect)[data->eid];
    event->signal();
}


void SYS_deleteEvent(void* ptr) {
    EvInfo* data = (EvInfo*) ptr;
    KernelEvent* event = (*Kernel::eventVect).get(data->eid);

    delete event;
}

