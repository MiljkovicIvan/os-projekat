#include <idle.h>
#include <pcb.h>
#include <dos.h>
#include <kernel.h>

#include <iostream.h><

#define IDLE_STACK_SIZE 512

void while1() {
    while(1) {}
}

IdleThread::IdleThread() {
    pcb = new PCB(IDLE_STACK_SIZE, 1); // timeSlice = 1

    pcb->initStack(0, while1);

    pcb->state = PCB_idle;
}

IdleThread::~IdleThread() {
    delete pcb;
}

void IdleThread::takeOver() {


#ifdef idle_debug
    lock_flag = 0;
    cout << "idle thread taking over" << endl;
    asm cli;
    lock_flag = 1;
#endif


    Kernel::mode = idle;

    Kernel::running = pcb;

    _SP = pcb->so;
    _SS = pcb->ss;

    asm {
        pop bp
        pop di
        pop si
        pop ds
        pop es
        pop dx
        pop cx
        pop bx
        pop ax

        iret
    }
}

