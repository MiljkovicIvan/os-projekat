#include <kersem.h>
#include <kernel.h>
#include <schedule.h>
#include <syscalls.h>
#include <pcb.h>

#include <iostream.h>
#include <debug.h>
#include <lock.h>

KerSem::KerSem(int init) {
    blocked = new Queue<PCB*>;
    value = init;
}

void KerSem::wait() {
#ifdef sem_debug
    lock_flag = 0;
    cout << "[SEM] wait. Val=" << value << " before wait" << endl;
    asm cli;
    lock_flag = 1;
#endif

    if (--value < 0) {
#ifdef sem_debug
        lock_flag = 0;
        cout << "[SEM-wait" << sid << "] " << Kernel::running->pid << " is blocked" << endl;
        asm cli;
        lock_flag = 1;
#endif
        blocked->put(Kernel::running);
        Kernel::running->state = PCB_blocked;
        //SYS_dispatch();
        //dispatching after every system call
    }

}

void KerSem::signal() {

#ifdef sem_debug
    lock_flag = 0;
    cout << "[SEM] signal. Val=" << value << " before signal" << endl;
    asm cli;
    lock_flag = 1;
#endif

    if (value++ < 0) {
        PCB* pcb = blocked->get();
#ifdef sem_debug
        lock_flag = 0;
        cout << "[SEM-sig" << sid << "] " << pcb->pid << "unblocked" << endl;
        asm cli;
        lock_flag = 1;
#endif
        Scheduler::put(pcb);

        if (pcb->state == PCB_blocked)
            pcb->state = PCB_ready;
        else
            cout << "[Semaphore-signal] PCB invalid state" << endl;
    }
}

KerSem::~KerSem() {
#ifdef sem_debug
    lock_flag = 0;
    cout << "deleting semaphore " << sid << endl;
    asm cli;
    lock_flag = 1;
#endif

    while(!blocked->empty()){
        PCB* pcb = blocked->get();
        Scheduler::put(pcb);

#ifdef sem_debug
        lock_flag = 0;
        cout << "returning pcb " << pcb->pid << "back in scheduler" << endl;
        asm cli;
        lock_flag = 1;
#endif

        if (pcb->state == PCB_blocked)
            pcb->state = PCB_ready;
        else
            cout << "[Semaphore-wait] PCB invalid state" << endl;
    }

    delete blocked;
}

