#include <kernev.h>
#include <types.h>
#include <kernel.h>
#include <schedule.h>
#include <pcb.h>
#include <iostream.h>
#include <IVTEntry.h>
#include <syscalls.h>
#include <iostream.h>
#include <debug.h>
#include <lock.h>

KernelEvent::KernelEvent(IVTNo _ivtno) {
    ivtno = _ivtno;
    creator = Kernel::running;
    val = 0;

    IVTEntry::IVT[ivtno]->setEvent(this);

#ifdef event_debug
    lock_flag = 0;
    cout << "[Event] " << ivtno << " entry" << endl << "[Event] created by thread " << creator->pid << endl;
    asm cli;
    lock_flag = 1;
#endif
}


KernelEvent::~KernelEvent() {

    IVTEntry::IVT[ivtno]->deleteEvent();

    if (val < 0) {
        lock
        creator->state = PCB_ready;
        Scheduler::put(creator);
        unlock
    }
}


void KernelEvent::signal() {
    if (val++ < 0){
        // change state of creator
        if (creator->state == PCB_blocked)
            creator->state = PCB_ready;
        else
            cout << "[KerEv-signal] PCB invalid state" << endl;
        Scheduler::put(creator);

#ifdef event_debug
        lock_flag = 0;
        cout << "[Event] " <<  ivtno << " signal " << "(val=" << val << ")" << endl;
        asm cli;
        lock_flag = 1;
#endif

    }

    if (val > 1)
        val = 1;
}


void KernelEvent::wait() {
    if (Kernel::running != creator)
        return;

    if (--val<0){
        if (creator->state == PCB_running)
            creator->state = PCB_blocked;
        else
            cout << "[KerEv-wait] PCB invalid state" << endl;

#ifdef event_debug
        lock_flag = 0;
        cout << "[Event] " <<  ivtno << " wait " << "(val=" << val << ")" << endl;
        asm cli;
        lock_flag = 1;
#endif

        //SYS_dispatch();
        //dispatching after every system call
    }

}

