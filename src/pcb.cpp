#include <pcb.h>
#include <types.h>
#include <dos.h>
#include <schedule.h>
#include <iostream.h>

PCB::PCB(StackSize s, Time t) {
    timeSlice = t;

    if (s > 0x8000)
        s = 0x8000;

    stackSize = s / sizeof(unsigned);

    stack = 0;

    state = PCB_created;
}

PCB::~PCB() {
    deinitStack();
}

void PCB::initStack(void* thread, void* run) {

    stack = new unsigned[stackSize]; // konju

    unsigned * sp = stack + stackSize;

    *(--sp) = FP_SEG(thread);
    *(--sp) = FP_OFF(thread);

    sp -= 2; // for callback

    *(--sp) = 0x0200; // pws
    *(--sp) = FP_SEG(run);
    *(--sp) = FP_OFF(run);

    sp -= 9; // for registers

    so = FP_OFF(sp);
    ss = FP_SEG(sp);

}

void PCB::deinitStack() {
    if (state != PCB_deleted) {
        state = PCB_deleted;
        delete [] stack;
    }
    else {
        cout << "[FATAL ERROR] PCB's deinitStack()" << endl;
    }
}

