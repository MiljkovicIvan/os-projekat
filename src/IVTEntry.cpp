#include <IVTEntry.h>
#include <dos.h>
#include <kernEv.h>
#include <lock.h>

IVTEntry* IVTEntry::IVT[256];


IVTEntry::IVTEntry(IVTNo _ivtno, pInterrupt _newInterrupt) {

    ivtno = _ivtno;
    newInterrupt = _newInterrupt;

    event = 0;

    lock
    oldInterrupt = getvect(ivtno);
    setvect(ivtno, newInterrupt);
    unlock

    IVT[ivtno] = this;

}

IVTEntry::~IVTEntry() {

    IVT[ivtno] = 0;

    lock
    setvect(ivtno, oldInterrupt);
    unlock

}

void IVTEntry::setEvent(KernelEvent* _event) {

    event = _event;

}

void IVTEntry::signalEvent() {

    if (event == 0)
        return;
    event->signal();

}

void IVTEntry::callOld() {

    (*oldInterrupt)();

}

void IVTEntry::deleteEvent() {

    lock
    setvect(ivtno, oldInterrupt);
    unlock

    IVT[ivtno] = 0;

    event = 0;

}

