//#include <usermain.h>
#include <kernel.h>

extern int userMain(int, char **);

int main(int argc, char ** argv) {

    Kernel::init();

    int returnValue = userMain(argc, argv);

    Kernel::clear();

    return returnValue;
}

