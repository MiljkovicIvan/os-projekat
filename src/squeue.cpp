#include <squeue.h>
#include <schedule.h>
#include <types.h>
#include <pcb.h>

SleepingQueue::SleepingQueue() {
    head = 0;
}

void SleepingQueue::put(PCB* pcb, Time timeToSleep) {

    if (head == 0) {
        Elem* elem = new Elem(pcb, timeToSleep);
        head = elem;
    } else {
        Elem* current = head, * previous = 0;
        Time time = timeToSleep;

        while (current && time > current->timeToSleep) {
            previous = current;
            time -= current->timeToSleep;
            current = current->next;
        }

        Elem* elem = new Elem(pcb, time);

        if (previous == 0) {
            elem->next = head;
            head = elem;
            elem->next->timeToSleep -= elem->timeToSleep;
        } else {
            elem->next = previous->next;
            previous->next = elem;

            if (elem->next)
                elem->next->timeToSleep -= elem->timeToSleep;
        }

    }
}

void SleepingQueue::update() {

    if (head) {
        head->timeToSleep--;
        while(head != 0 && head->timeToSleep == 0) {
            Elem* tmp = head;
            head = head->next;
            Scheduler::put(tmp->pcb);

#ifdef thread
            lock_flag = 0;
            cout << tmp->pcb->pid << " waking up" << endl;
            asm cli;
            lock_flag = 1;
#endif

            delete tmp;
        }
    }

}

