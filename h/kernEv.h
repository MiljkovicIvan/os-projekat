#ifndef kernev_h
#define kernev_h

#include <types.h>
class PCB;

class KernelEvent {
    IVTNo ivtno;
    PCB* creator;
    int val;
public:
    KernelEvent(IVTNo);
    ~KernelEvent();

    void signal();
    void wait();
};

#endif

