#ifndef event_h
#define event_h

#include <types.h>
#include <IVTEntry.h>
#include <kernel.h>

class KernelEv;


#define PREPAREENTRY(num, call_old)\
void interrupt intr##num(...);\
IVTEntry entry##num(num, &intr##num);\
void interrupt intr##num(...)\
{\
    entry##num.signalEvent();\
    if (call_old) entry##num.callOld();\
    if (Kernel::mode == kernel)\
        Kernel::eventFlag = 1;\
    else\
        dispatch();\
}


class Event {
public:
    Event(IVTNo ivtno);
    ~Event();

    void wait();

protected:
    friend class KernelEv;
    void signal();

private:
    ID eid;
};

#endif

