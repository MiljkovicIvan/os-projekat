#ifndef queue_h
#define queue_h

#include <iostream.h>

template <class T>
class Queue {
private:
    struct Elem {
        T info;
        Elem * next;
        Elem(T t) {
            info = t;
            next = 0;
        }
    };

    Elem* head, * tail;

public:
    Queue() {
        head = tail = 0;
    }

    ~Queue() {
        Elem* temp;
        while(temp = head) {
            delete temp;
            head = head->next;
        }
    }

    void put(T t) {
        Elem* _new = new Elem(t);
        if (tail == 0)
            head = tail = _new;
        else{
            tail->next = _new;
            tail = _new;
        }
    }

    T get() {
        Elem* temp = head;
        if (temp == 0) {
            cout << "******** QUEUE UNDERFLOW ********" << endl;
        } else {
            head = head->next;
            if (head == 0)
                tail = 0;
            return temp->info;
        }
    }

    int empty() {
        if (head == 0)
            return 1;
        else
            return 0;
    }

}

#endif

