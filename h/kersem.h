#ifndef kersem_h
#define kersem_h

#include <queue.h>
#include <pcb.h>
#include <types.h>

class KerSem {

public:
    Queue<PCB*>* blocked;
    int value;
    ID sid;


    KerSem(int init);
    ~KerSem();


    void wait();
    void signal();

};

#endif

