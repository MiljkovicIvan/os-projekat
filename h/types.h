#ifndef __TYPES_H__
#define __TYPES_H__


typedef unsigned int StackSize;
const StackSize defaultStackSize = 1024;


typedef unsigned int Time;
const Time defaultTimeSlice = 2; // 2x55ms


typedef int ID;


typedef unsigned char IVTNo;


typedef void interrupt (*pInterrupt)(...);

typedef void (*pFun)(void*);


const char callIDs[][20] = {
    "newThread",
    "startThread",
    "waitToComplete",
    "sleepThread",
    "endThread",
    "dispatch",
    "new Semaphore",
    "Semaphore wait",
    "Semaphore signal",
    "Semaphor Value",
    "delete Semaphore"
};

enum sysID {
    sysID_newThread = 0,
    sysID_startThread,
    sysID_waitToComplete,
    sysID_sleepThread,
    sysID_endThread,
    sysID_dispatch,

    sysID_newSem,
    sysID_SemWait,
    sysID_SemSig,
    sysID_SemVal,
    sysID_delSem,

    sysID_newEv,
    sysID_waitEv,
    sysID_sigEv,
    sysID_delEv

};

struct ThreadInfo {
    StackSize stackSize;
    Time timeSlice;
    void* myThread;
    void* run;
    ID tid;
};

struct SemInfo {
    int value;
    ID sid;
};

struct EvInfo {
    ID eid;
    IVTNo ivtno;
};

#endif
