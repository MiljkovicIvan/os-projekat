#ifndef ivtentry_h
#define ivtentry_h

#include <types.h>

class KernelEvent;

class IVTEntry {
    IVTNo ivtno;
    KernelEvent* event;
    pInterrupt oldInterrupt, newInterrupt;
public:

    static IVTEntry* IVT[256];

    IVTEntry(IVTNo, pInterrupt);
    ~IVTEntry();

    void setEvent(KernelEvent*);
    void deleteEvent();
    void signalEvent();
    void callOld();

};

#endif

