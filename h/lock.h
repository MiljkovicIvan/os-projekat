#ifndef lock_h
#define lock_h

extern volatile int lock_flag;
extern volatile int switch_context_request;

extern void dispatch();

#define lock lock_flag = 0;
#define unlock lock_flag = 1;\
               if (switch_context_request)\
                    dispatch();

#endif

