#ifndef vector_h
#define vector_h

#include <iostream.h>

template <class T>
class Vector {
private:
    struct Elem {
        int index;
        Elem* next;
        Elem(int _index) {
            index = _index;
            next = 0;
        }
    };


    Elem * head, * tail;

    T* arr;
    int cursor;
    int size;

    void insertInFreeEntryList(Elem* current) {
            if (head == 0) {
                head = tail = current;
            } else {
                current->next = head;
                head = current;
            }
    }

    int findIndex() {
        if (head) {
            Elem* temp = head;

            head = head->next;
            if (head == 0)
                tail = 0;

            int ret = temp->index;
            delete temp;

            return ret;
        }

        return cursor++;
    }

public:
    Vector(int _size = 200) {
        size = _size;
        head = tail = 0;
        cursor = 0;
        arr = new T[size];
    }

    ~Vector() {
        delete [] arr;
        Elem* old;
        while(head) {
            old = head;
            head = head->next;
            delete old;
        }
    }

    int put(T& t) {
        int index = findIndex();

        if (index == size) { // vector is full
            cout << "[FatalEror] Vector is full" << endl;
        }

        arr[index] = t;

        return index;
    }

    T operator[](int index) {
        return arr[index];
    }

    T& get(int index) {
        Elem* temp = new Elem(index);
        insertInFreeEntryList(temp);
        return arr[index];
    }

    void pisi() {
    }

};

#endif
