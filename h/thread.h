#ifndef __THREAD_H__
#define __THREAD_H__

#include "types.h"

class PCB;

class Thread {

public:
    void start();
    void waitToComplete();
    virtual ~Thread();
    static void sleep(Time tomeToSleep);

protected:
    friend class PCB;
    Thread(StackSize stackSize = defaultStackSize, Time timeSlice = defaultTimeSlice);
    virtual void run() {}

private:
    ID tid; // ovo treba da bude privatno, testiranje u toku
    static void wrapper(Thread * running);
};

void dispatch();

#endif

