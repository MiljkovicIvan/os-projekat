#ifndef semaphore_h
#define semaphore_h

#include <types.h>

class KerSem;

class Semaphore {
public:
    Semaphore(int init=1);
    virtual ~Semaphore();

    virtual void wait();
    virtual void signal();

    int val() const;

private:
    ID sid;
};

#endif

