#ifndef pcb_h
#define pcb_h

#include <types.h>
#include <queue.h>

enum PCBState { PCB_created, PCB_blocked, PCB_ready, PCB_finished, PCB_idle, PCB_running, PCB_deleted };

const char PCBNames[][20] = {
    "created",
    "blocked",
    "ready",
    "finished",
    "idle",
    "running",
    "deleted"
};

class PCB {
public:
    Time timeSlice;
    StackSize stackSize;

    unsigned ss, so; // stack_segment & stack_offset
    unsigned * stack;
    ID pid;

    PCBState state;

    Queue<PCB*> blocked;


    PCB(StackSize s, Time t);
    ~PCB();
    void initStack(void * thread, void * run);
    void deinitStack();
};

#endif

