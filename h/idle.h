#ifndef idle_h
#define idle_h

#include <pcb.h>

class IdleThread {
public:
    IdleThread();

    PCB * pcb;

    void takeOver();

    ~IdleThread();

};

#endif

