#ifndef kernel_h
#define kernel_h

#include <pcb.h>
#include <vector.h>
#include <kthread.h>
#include <idle.h>
#include <squeue.h>
#include <types.h>
#include <kersem.h>
#include <kernev.h>
#include <kernev.h>

enum KernelState {kernel, notKernel, idle};

class Kernel {
public:

    static int eventFlag;

    static KernelState mode;

    static KernelThread* kernelThread;
    static IdleThread* idleThread;

    static PCB* running;
    static PCB* userMain;

    static void* oldTimer;
    static int tick;

    static SleepingQueue * sleepingQueue;

    static Vector<PCB*>* pcbVect;
    static Vector<KerSem*>* semVect;
    static Vector<KernelEvent*>* eventVect;

    static void init();
    static void clear();

    static void switchContext();
    static void callRoutine(unsigned callID, void* data);

    static pFun* funs;

};

#endif