#ifndef squeue_h
#define squeue_h

#include <pcb.h>
#include <types.h>

class SleepingQueue {
private:
    struct Elem {
        PCB* pcb;
        Elem* next;
        Time timeToSleep;

        Elem (PCB* _pcb, Time _timeToSleep) {
            pcb = _pcb;
            next = 0;
            timeToSleep = _timeToSleep;
        }
    };

    Elem* head;

public:

    SleepingQueue();

    void put(PCB* pcb, Time timeToSleep);

    void update();
};

#endif

