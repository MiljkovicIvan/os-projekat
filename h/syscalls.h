#ifndef syscalls_h
#define syscalls_h

#include <types.h>


void SYS_newThread(void*);

void SYS_startThread(void*);

void SYS_waitToComplete(void*);

void SYS_sleepThread(void*);

void SYS_endThread(void*);

void SYS_dispatch(void*); //void* because of function array



void SYS_newSemaphore(void*);

void SYS_SemaphoreWait(void*);

void SYS_SemaphoreSignal(void*);

void SYS_SemaphoreVal(void*);

void SYS_deleteSemaphore(void*);



void SYS_newEvent(void*);

void SYS_waitEvent(void*);

void SYS_signalEvent(void*);

void SYS_deleteEvent(void*);


#endif

