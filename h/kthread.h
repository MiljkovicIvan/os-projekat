#ifndef kthread_h
#define kthread_h

#include <pcb.h>
#include <types.h>


class KernelThread {

public:

    KernelThread();
    void takeOver(unsigned callID, unsigned seg, unsigned off);
    ~KernelThread();

private:
    PCB * pcb;
    friend class PCB;
    StackSize stackSize;

};




#endif

